#!/bin/bash
#mkdir -p /home/autobuilder/pokybuild3/yocto-worker/trash
mkdir -p /srv/autobuilder/dsautobuilder-share/repos
mkdir -p /srv/autobuilder/dsautobuilder-share/current_sources
mkdir -p /srv/autobuilder/dsautobuilder-share/pub
chown autobuilder /home/autobuilder/pokybuild3/yocto-worker
chown autobuilder /home/autobuilder/pokybuild3/yocto-controller
chown autobuilder /srv/autobuilder/*
chown autobuilder /srv/autobuilder/dsautobuilder-share/*
chown -R autobuilder /opt/mender-binary-delta*/keys
sudo -H -u autobuilder bash -c "cd /home/autobuilder/yocto-autobuilder-helper;git pull"
cd /home/autobuilder/pokybuild3
case $AB_MODE in	
	controller*)
		echo ///Init dirs for controller mode
		if [ ! -e yocto-controller/.setupdone ];then
			sudo -H -u autobuilder bash -c "rsync -av yocto-controller-template/* yocto-controller/"
			sudo -H -u autobuilder bash -c "touch yocto-controller/.setupdone"
		else
			echo ///already initialized
			if [ ! -e yocto-controller/.nosync];then
				sudo -H -u autobuilder bash -c "rsync -av yocto-controller-template/yoctoabb yocto-controller/"
				sudo -H -u autobuilder bash -c "rsync -av yocto-controller-template/buildbot.tac yocto-controller/"
			fi
		fi
		;;
	worker)
		echo ///Init dirs for worker mode
		if [ ! -e yocto-worker/.setupdone ];then
			sudo -H -u autobuilder bash -c "rsync -av yocto-worker-template/* yocto-worker/"
			sudo -H -u autobuilder bash -c "touch yocto-worker/.setupdone"
		else
			echo ///already initialized
			if [ ! -e yocto-worker/.nosync];then
				sudo -H -u autobuilder bash -c "rsync -av yocto-worker-template/buildbot.tac yocto-worker/"
			fi
		fi
		sed -i -e "/buildmaster_host/s/ds-autobuilder-controller/$AB_CONTROLLER_ADDR/" yocto-worker/buildbot.tac
		sed -i -e "/port/s/9989/$AB_CONTROLLER_PORT/" yocto-worker/buildbot.tac
		sed -i -e "/workername/s/ubuntu1804-ty-1/$AB_WORKER_NAME/" yocto-worker/buildbot.tac
		;;
esac
exit 0
